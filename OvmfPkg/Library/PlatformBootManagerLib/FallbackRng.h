/** @file
  Copyright (C) 2024, Red Hat, Inc.
  SPDX-License-Identifier: BSD-2-Clause-Patent
**/

#ifndef _FALLBACK_RNG_H_
#define _FALLBACK_RNG_H_

#include <Uefi/UefiBaseType.h>
#include <Uefi/UefiSpec.h>

EFI_STATUS
FallbackRngCheckAndInstall (
  );

VOID
FallbackRngPrintWarning (
  );

#endif
